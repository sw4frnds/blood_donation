from flask import url_for, request, render_template, redirect, flash, session
from app import app
from functools import wraps
from models import *
from forms import *

@app.route('/')
def home():
	
	return render_template("index1.html")

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' and  'user' in session:
            
            return f(*args, **kwargs)
    	else:
    		flash("you need to log_in first!!")
        	return redirect(url_for('orglogin'))
		# return redirect(url_for('orglogin')    	 
    return wrap

@app.route('/register/', methods=['GET','Post'])
def register():
	# form = user_sign_up
	try:
		form = userreg(request.form)
		if request.method == 'POST' and form.validate():
			register = users(form.name.data,
			form.ph_no.data,
			form.address.data,
			form.city.data,
			form.dob.data,
			form.blood_group.data)
		
			db.session.add(register)
			db.session.commit()
			flash("thanx for registering")
			return redirect(url_for('home'))
		return render_template('register.html',form=form)
	except Exception as e:
		return (str(e))
	# return render_template('register.html',form=form)

@app.route('/orglogin/', methods=['GET','POST'])
def orglogin():
	
	form = login(request.form)
	if request.method == 'POST' and form.validate():
		data = org.query.filter_by(ph_no = form.ph_no.data).first()
		if org.query.filter_by(ph_no = form.ph_no.data).first() and data.password == form.password.data:
			flash("you are now logged in..!!")
			# flash(data.name)
			session['logged_in'] = True
			session['user'] = data.name
			flash(session['user'])
			
			return redirect(url_for('event_creation'))
		else:
			flash("invalid credentials")
			return render_template('orglogin.html',form=form)
	else:
		return render_template('orglogin.html',form=form)



@app.route('/logout/')
@login_required
def logout():
	session.clear()
	flash("you are logged out.")
	return redirect(url_for('home'))
	
@app.route('/event_creation/', methods=['GET','POST'])
@login_required
def event_creation():

	form = event(request.form)
	if request.method == "POST" and form.validate():
		create = events(form.title.data,
		form.venue.data,
		form.event_desc.data,
		form.blood_bank.data,
		form.event_time.data)
		
		db.session.add(create)
		db.session.commit()
		flash('Event created Sucessfully')
		return redirect(url_for('home'))
	return render_template('event-creation.html',form=form)

	# return render_template('event-creation.html')


@app.route('/org_sign_up/', methods=['GET','POST'])
def org_sign_up():
	
	form = orgsignup(request.form)
	if request.method == "POST" and form.validate():
		register = org(form.org_name.data,
		form.org_type.data,
		form.address.data,
		form.city.data,
		form.ph_no.data,
		form.password.data)
	
		db.session.add(register)
		db.session.commit()
		return redirect(url_for('home'))
	return render_template('orgsignup.html',form=form)
# except Exception as e:
# 	return (str(e))


@app.route('/contact/')
def contact():
	# myUser = users.query.filter_by(ph_no = user).first()
	return render_template('contact-us.html')

@app.route('/donors/')
def donors():
	# myUser = users.query.filter_by(ph_no = user).first()
	return render_template('donors.html')

@app.route('/gallery/')
def gallery():
	# myUser = users.query.filter_by(ph_no = user).first()
	return render_template('gallery.html')

	
@app.route('/users/')
def Users():
	# myUser = users.query.filter_by(ph_no = user).first()
	return render_template('p2.html', myUser = myUser)

@app.route('/search_registered_users/', methods=['GET','POST'])
@login_required
def search_registered_users():
	try:
		form = search(request.form)
		if request.method == 'POST' and form.validate():			
			ph_no = form.ph_no.data
			
			if users.query.filter_by(ph_no=ph_no).first():
				flash("user exist")
				value = users.query.filter_by(ph_no=ph_no)
				return render_template('search_registered_users.html',form = form, value=value)
			else:
				flash('sorry!!')
				return render_template('search_registered_users.html',form = form)	
		else:
			return render_template('search_registered_users.html',form = form)

	except Exception as error:
		return render_template('search_registered_users.html',error = error)

@app.errorhandler(404)
def page_not_found(e):
	error = e
	flash("invalid request !!!")
	return render_template('404.html', error = error)