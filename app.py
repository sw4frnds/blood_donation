from flask import Flask
# from models import *


app = Flask(__name__)
from views import *
from models import db

if __name__ == '__main__':
	app.debug = True
	app.secret_key = "hello"
	app.run(host='127.0.0.1')
