from flask_sqlalchemy  import SQLAlchemy
from app import app
# from sqlalchemy.dialects.postgresql import JSON

# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://team2:team123@http://10.11.1.10/pg_admin:5432/team2'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://shubham:shubham124@localhost/blood_donation'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:shubham@localhost/blood_donation'
db = SQLAlchemy(app)

class users(db.Model):
	u_id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(30), nullable=False)
	ph_no = db.Column(db.BigInteger, unique=True)
	address = db.Column(db.String(80), nullable=False)
	city = db.Column(db.String(15), nullable=False)
	dob = db.Column(db.DATE, nullable=False)
	blood_group = db.Column(db.String(3), nullable=False)
	# password = db.Column(db.String(100),nullable=False)

	def __init__(self,name,ph_no,address,city,dob,blood_group):
		# self.u_id = u_id
		self.name = name
		self.ph_no = ph_no
		self.address = address
		self.city = city
		self.dob = dob
		self.blood_group = blood_group

	# def __repr__(self):
	# 	return '%r' % (self.u_id)
class org(db.Model):
	org_id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(30), nullable=False)
	org_type = db.Column(db.String(80), nullable=False)
	address = db.Column(db.String(80), nullable=False)
	city = db.Column(db.String(15), nullable=False)
	ph_no = db.Column(db.BigInteger, unique=True)
	# blood_group = db.Column(db.String(3), nullable=False)
	password = db.Column(db.String(100),nullable=False)

	def __init__(self,name,org_type,address,city,ph_no,password):
		# self.u_id = u_id
		self.name = name
		self.org_type = org_type
		self.address = address
		self.city = city
		self.ph_no = ph_no
		self.password = password

class events(db.Model):
	e_id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(50), default='Blood Donation Camp')
	venue = db.Column(db.String(50), nullable=False)
	event_desc = db.Column(db.String(100), nullable=False)
	blood_bank = db.Column(db.String(15), nullable=False)
	event_time = db.Column(db.DateTime ,nullable=True)
	# blood_group = db.Column(db.String(3), nullable=False)
	# password = db.Column(db.String(100),nullable=False)

	def __init__(self,title,venue,event_desc,blood_bank,event_time):
		# self.u_id = u_id
		self.title = title
		self.venue = venue
		self.event_desc = event_desc
		self.blood_bank = blood_bank
		self.event_time = event_time