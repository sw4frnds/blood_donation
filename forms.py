from wtforms import Form, validators
from flask.ext.admin.form import DateTimePickerWidget
from wtforms import TextField, DateField, DateTimeField, IntegerField, SelectField, TextAreaField, PasswordField
from wtforms.validators import required, EqualTo, Length


class userreg(Form):
  name = TextField('Name',[validators.required(), validators.Length(min=3, max=20)])
  ph_no = IntegerField('Phone Number',[validators.required()])
  address = TextAreaField('Address',[validators.required(), validators.Length(max=100)])
  city = SelectField('City',[validators.required()],choices=[('Chandigarh','Chandigarh'),('Ludhiana','Ludhiana')])
  dob = DateField('Date Of Birth',[validators.required()],format='%d/%m/%Y')
  blood_group = SelectField('Blood Group',[validators.required()],choices=[('A+','A+'),('A-','A-'),('B+','B+'),('B-','B-'),('AB+','AB+'),('AB-','AB-'),('O+','O+'),('O-','O-')])

class orgsignup(Form):
  org_name = TextField('Organisation Name',[validators.required(), validators.Length(min=3, max=20)])
  org_type = TextField('Organisation Type',[validators.required()])
  address = TextAreaField('Address',[validators.required(), validators.Length(max=100)])
  city = SelectField('City',[validators.required()],choices=[('Chandigarh','Chandigarh'),('Ludhiana','Ludhiana')])
  ph_no = IntegerField('Phone Number',[validators.required()])
  password = PasswordField('password', [
        validators.DataRequired(), validators.Length(min=4, max=20),
        validators.EqualTo('c_password', message='Passwords must match')
    ])
  c_password = PasswordField('Repeat Password')
  
class login(Form):
  ph_no = IntegerField('Phone Number',[validators.required()])
  password = PasswordField('password',[validators.required()])

class event(Form):
  title = TextField('Event Title',[validators.required()])
  venue = TextField('venue',[validators.required()])
  event_desc = TextAreaField('Event Description',[validators.required(), validators.Length(max=100)])
  blood_bank = SelectField('Blood Bank',[validators.required()],choices=[('blood bank 1','blood bank 1'),('blood bank 2','blood bank 2')])
  event_time = DateTimeField('Event Date & Time',format='%d/%m/%Y %H:%M')
  # blood_group = SelectField('Blood Group',[validators.required()],choices=[('A+','A+'),('A-','A-'),('B+','B+'),('B-','B-'),('AB+','AB+'),('AB-','AB-'),('O+','O+'),('O-','O-')])

class search(Form):
  ph_no = IntegerField('Phone Number',[validators.required()])
  # password = PasswordField('password',[validators.required()])